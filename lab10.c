#include <stdio.h>
#include <stdlib.h>
int main()
{
    char sentence[100];

    FILE *fptr;

    fptr = fopen("input", "w");


    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }
    printf("Enter a sentence:\n");
    fgets(sentence, sizeof(sentence), stdin);
    fprintf(fptr, "%s", sentence);
    fptr=fopen("input","r");
    fscanf(fptr, "%[^\n]", sentence);
    printf("Data from the file:\n%s", sentence);
    fclose(fptr);
    return 0 ;
}//write your code here
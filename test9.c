#include <stdio.h>
int main()
{
   int  first, second, *p, *q, sum , product , difference  ;
   int remainder ;
   float quotient ;
   printf("Enter two integers \n");
   scanf("%d%d", &first, &second);

   p = &first;
   q = &second;

   sum = (*p) + (*q);
   difference= (*p)-(*q) ;
   product = (*p)*(*q) ;
   quotient = (*p)/(*q) ;
   remainder =(*p)%(*q) ;

   printf("Sum of the numbers %d and %d  = %d\n",*p,*q, sum);
   printf("Difference of the numbers %d and %d = %d\n",*p,*q, difference);
   printf("product  the numbers %d and %d = %d\n",*p,*q, product);
   printf("Quotient of the numbers %d and %d = %f\n",*p,*q, quotient);
   printf("Remainder of the numbers %d and %d = %d\n",*p,*q,remainder);
   return 0;
}//write your code here

1. FACTORIAL OF A NUMBER


#include<stdio.h>
int main()
{
int fact=1;
int n,i;
printf("enter the numbers\n");
scanf("%d",&n);
if(n==0)
{
fact=1;
}
else{
for(i=1;i<=n;i++)
fact=fact*i;
}
printf("the factorial of a number=%d\n",fact);
return 0;
}


2.TO PRINT REVERSE OF 3 DIGIT NUMBER

#include<stdio.h>
int main()
{
int temp,num;
printf("enter the number\n");
scanf("%d",&num);
while(num==0)
{
temp=num%10;
printf("%d\t",temp);
num=num/10;
}
return 0;
}


4. PATTERN TO PRINT 
1
22
333
4444
55555

#include<stdio.h>
int main()
{
int i,j;
for(i=1;i<6;i++)
{
printf("\n");
for(j=1;j<=i;j++)
printf("%d",i);
}
return 0;
}


5.PATTERN TO PRINT
A           WHERE A=65,B=66,C=67,D=68,E=69,F=70
AB
ABC
ABCD
ABCDE
ABCDEF

#include<stdio.h>
int main()
{
int i,j;
char ch;
for(i=65;i<=70;i++)
{
printf("\n");
for(j=65;j<=i;j++)
printf("%c",j);
}
return 0;
}


6.TO CALCULATE GCD OF TWO NUMBERS.

#include<stdio.h>
int main()
{
int num1,num2,divisor,dividend,reminder;
printf("enter two numbers\n");
scanf("%d%d",&num1,&num2);
if(num1>num2)
{
dividend=num1;
divisor=num2;
}
else{
dividend=num2;
divisor=num1;
}
while(divisor)
{
reminder=dividend%divisor;
dividend=divisor;
divisor=reminder;
}
printf("the gcd of two numbers is %d%d=%d\n",num1,num2,dividend);
return 0;
}


3.SUM OF SERIES

#include <stdio.h>
#include<math.h>
double series(int n) 
{ 
    int i; 
    double sums = 0.0, ser; 
    for (i = 1; i <= n; ++i) 
    { 
        ser = 1 / pow(i, i); 
        sums += ser; 
    } 
    return sums; 
} 

int main() 
{ 
    int n ;
    printf("Enter the number of terms") ;
    scanf("%d",&n); 
    double res = series(n); 
    printf("The sum of series is %lf\n", res); 
    return 0; 
} 





2.
#include <stdio.h>
int main()
{
   int x, y, *a, *b, temp;
   
   printf("Enter the value of x and y\n");
   scanf("%d%d", &x, &y);
 
   printf("Before Swapping\nx = %d\ny = %d\n", x, y);
 
   a = &x;
   b = &y;
 
   temp = *b;
   *b = *a;
   *a = temp;
 
   printf("After Swapping\n x = %d\n y = %d \n", x, y);
 
   return 0;
}

1.

#include <stdio.h>
int main()
{
    char *ch;    
    printf("Enter a character: ");
    scanf("%c",ch);
    
      if((*ch>='A' && *ch<='Z') || (*ch>='a' && *ch<='z'))
    {
           switch(*ch)
        {
            case 'A':
            case 'E':
            case 'I':
            case 'O':
            case 'U':
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                printf("%c is a Vowel \n",*ch);
                break;
            default:
                printf("%c is a not a vowel but a Consonant\n",*ch);            
        }
    }
     else
    {
        printf("%c is not an alphabet,Please enter a valid character\n",*ch);
    }
    return 0;
}
//write your code here